﻿namespace BGCAProductUpdate
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtResponse = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvProductitem = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkAsgPurToGrp = new System.Windows.Forms.CheckBox();
            this.btnUpdateInfo = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtStdPrice = new System.Windows.Forms.TextBox();
            this.cmbGroupRole = new System.Windows.Forms.ComboBox();
            this.cmbGroup = new System.Windows.Forms.ComboBox();
            this.dtTermEndDate = new System.Windows.Forms.DateTimePicker();
            this.dtTermStartDate = new System.Windows.Forms.DateTimePicker();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dtFilterEffectiveEndDate = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.dtFilterEffectiveStartDate = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.btnFilter = new System.Windows.Forms.Button();
            this.txtNameFilter = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblUpdatingDatabaseName = new System.Windows.Forms.Label();
            this.lblAsibaseUrl = new System.Windows.Forms.Label();
            this.chkSelectAll = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductitem)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtResponse
            // 
            this.txtResponse.Location = new System.Drawing.Point(12, 436);
            this.txtResponse.Multiline = true;
            this.txtResponse.Name = "txtResponse";
            this.txtResponse.Size = new System.Drawing.Size(725, 202);
            this.txtResponse.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 420);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Processing Info";
            // 
            // dgvProductitem
            // 
            this.dgvProductitem.AllowUserToAddRows = false;
            this.dgvProductitem.AllowUserToDeleteRows = false;
            this.dgvProductitem.AllowUserToResizeRows = false;
            this.dgvProductitem.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvProductitem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProductitem.Location = new System.Drawing.Point(12, 199);
            this.dgvProductitem.MultiSelect = false;
            this.dgvProductitem.Name = "dgvProductitem";
            this.dgvProductitem.Size = new System.Drawing.Size(1094, 215);
            this.dgvProductitem.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 160);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Product data";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkAsgPurToGrp);
            this.groupBox1.Controls.Add(this.btnUpdateInfo);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtStdPrice);
            this.groupBox1.Controls.Add(this.cmbGroupRole);
            this.groupBox1.Controls.Add(this.cmbGroup);
            this.groupBox1.Controls.Add(this.dtTermEndDate);
            this.groupBox1.Controls.Add(this.dtTermStartDate);
            this.groupBox1.Location = new System.Drawing.Point(743, 420);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(363, 218);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Update Selected Products To";
            // 
            // chkAsgPurToGrp
            // 
            this.chkAsgPurToGrp.AutoSize = true;
            this.chkAsgPurToGrp.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkAsgPurToGrp.Checked = true;
            this.chkAsgPurToGrp.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAsgPurToGrp.Location = new System.Drawing.Point(148, 33);
            this.chkAsgPurToGrp.Name = "chkAsgPurToGrp";
            this.chkAsgPurToGrp.Size = new System.Drawing.Size(156, 17);
            this.chkAsgPurToGrp.TabIndex = 12;
            this.chkAsgPurToGrp.Text = "Assign Purchaser To Group";
            this.chkAsgPurToGrp.UseVisualStyleBackColor = true;
            this.chkAsgPurToGrp.CheckedChanged += new System.EventHandler(this.chkAsgPurToGrp_CheckedChanged);
            // 
            // btnUpdateInfo
            // 
            this.btnUpdateInfo.Location = new System.Drawing.Point(147, 188);
            this.btnUpdateInfo.Name = "btnUpdateInfo";
            this.btnUpdateInfo.Size = new System.Drawing.Size(154, 23);
            this.btnUpdateInfo.TabIndex = 10;
            this.btnUpdateInfo.Text = "Update Products";
            this.btnUpdateInfo.UseVisualStyleBackColor = true;
            this.btnUpdateInfo.Click += new System.EventHandler(this.btnUpdateInfo_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 165);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Standard Price (Default)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 142);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Term End Date";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Term Start Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Group Role";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Group";
            // 
            // txtStdPrice
            // 
            this.txtStdPrice.Location = new System.Drawing.Point(147, 162);
            this.txtStdPrice.Name = "txtStdPrice";
            this.txtStdPrice.Size = new System.Drawing.Size(200, 20);
            this.txtStdPrice.TabIndex = 4;
            this.txtStdPrice.Validating += new System.ComponentModel.CancelEventHandler(this.txtStdPrice_Validating);
            // 
            // cmbGroupRole
            // 
            this.cmbGroupRole.FormattingEnabled = true;
            this.cmbGroupRole.Location = new System.Drawing.Point(147, 83);
            this.cmbGroupRole.Name = "cmbGroupRole";
            this.cmbGroupRole.Size = new System.Drawing.Size(201, 21);
            this.cmbGroupRole.TabIndex = 3;
            // 
            // cmbGroup
            // 
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Location = new System.Drawing.Point(147, 56);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(201, 21);
            this.cmbGroup.TabIndex = 2;
            this.cmbGroup.SelectedIndexChanged += new System.EventHandler(this.cmbGroup_SelectedIndexChanged);
            // 
            // dtTermEndDate
            // 
            this.dtTermEndDate.Location = new System.Drawing.Point(148, 136);
            this.dtTermEndDate.Name = "dtTermEndDate";
            this.dtTermEndDate.Size = new System.Drawing.Size(200, 20);
            this.dtTermEndDate.TabIndex = 1;
            // 
            // dtTermStartDate
            // 
            this.dtTermStartDate.Location = new System.Drawing.Point(148, 110);
            this.dtTermStartDate.Name = "dtTermStartDate";
            this.dtTermStartDate.Size = new System.Drawing.Size(200, 20);
            this.dtTermStartDate.TabIndex = 0;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dtFilterEffectiveEndDate);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.dtFilterEffectiveStartDate);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.btnFilter);
            this.groupBox2.Controls.Add(this.txtNameFilter);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(12, 53);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1094, 92);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Update Simple Products";
            // 
            // dtFilterEffectiveEndDate
            // 
            this.dtFilterEffectiveEndDate.Location = new System.Drawing.Point(711, 29);
            this.dtFilterEffectiveEndDate.Name = "dtFilterEffectiveEndDate";
            this.dtFilterEffectiveEndDate.ShowCheckBox = true;
            this.dtFilterEffectiveEndDate.Size = new System.Drawing.Size(200, 20);
            this.dtFilterEffectiveEndDate.TabIndex = 15;
            this.dtFilterEffectiveEndDate.Value = new System.DateTime(2020, 6, 1, 10, 23, 30, 0);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(608, 32);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Effective End Date";
            // 
            // dtFilterEffectiveStartDate
            // 
            this.dtFilterEffectiveStartDate.Location = new System.Drawing.Point(402, 29);
            this.dtFilterEffectiveStartDate.Name = "dtFilterEffectiveStartDate";
            this.dtFilterEffectiveStartDate.ShowCheckBox = true;
            this.dtFilterEffectiveStartDate.Size = new System.Drawing.Size(200, 20);
            this.dtFilterEffectiveStartDate.TabIndex = 13;
            this.dtFilterEffectiveStartDate.Value = new System.DateTime(2020, 6, 1, 10, 23, 14, 0);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(296, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Effective Start Date";
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(48, 56);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(117, 23);
            this.btnFilter.TabIndex = 11;
            this.btnFilter.Text = "Filter Products";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // txtNameFilter
            // 
            this.txtNameFilter.Location = new System.Drawing.Point(90, 29);
            this.txtNameFilter.Name = "txtNameFilter";
            this.txtNameFilter.Size = new System.Drawing.Size(200, 20);
            this.txtNameFilter.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Name Contains";
            // 
            // lblUpdatingDatabaseName
            // 
            this.lblUpdatingDatabaseName.AutoSize = true;
            this.lblUpdatingDatabaseName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpdatingDatabaseName.Location = new System.Drawing.Point(9, 22);
            this.lblUpdatingDatabaseName.Name = "lblUpdatingDatabaseName";
            this.lblUpdatingDatabaseName.Size = new System.Drawing.Size(174, 22);
            this.lblUpdatingDatabaseName.TabIndex = 6;
            this.lblUpdatingDatabaseName.Text = "Updating Database :";
            // 
            // lblAsibaseUrl
            // 
            this.lblAsibaseUrl.AutoSize = true;
            this.lblAsibaseUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAsibaseUrl.Location = new System.Drawing.Point(179, 22);
            this.lblAsibaseUrl.Name = "lblAsibaseUrl";
            this.lblAsibaseUrl.Size = new System.Drawing.Size(0, 22);
            this.lblAsibaseUrl.TabIndex = 7;
            // 
            // chkSelectAll
            // 
            this.chkSelectAll.AutoSize = true;
            this.chkSelectAll.Location = new System.Drawing.Point(15, 176);
            this.chkSelectAll.Name = "chkSelectAll";
            this.chkSelectAll.Size = new System.Drawing.Size(70, 17);
            this.chkSelectAll.TabIndex = 8;
            this.chkSelectAll.Text = "Select All";
            this.chkSelectAll.UseVisualStyleBackColor = true;
            this.chkSelectAll.CheckedChanged += new System.EventHandler(this.chkSelectAll_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1165, 715);
            this.Controls.Add(this.chkSelectAll);
            this.Controls.Add(this.lblAsibaseUrl);
            this.Controls.Add(this.lblUpdatingDatabaseName);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgvProductitem);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtResponse);
            this.Name = "Form1";
            this.Text = "Update Simple Products";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductitem)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtResponse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvProductitem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnUpdateInfo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtStdPrice;
        private System.Windows.Forms.ComboBox cmbGroupRole;
        private System.Windows.Forms.ComboBox cmbGroup;
        private System.Windows.Forms.DateTimePicker dtTermEndDate;
        private System.Windows.Forms.DateTimePicker dtTermStartDate;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNameFilter;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.DateTimePicker dtFilterEffectiveStartDate;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtFilterEffectiveEndDate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chkAsgPurToGrp;
        private System.Windows.Forms.Label lblUpdatingDatabaseName;
        private System.Windows.Forms.Label lblAsibaseUrl;
        private System.Windows.Forms.CheckBox chkSelectAll;
    }
}

