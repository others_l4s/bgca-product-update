﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BGCAProductUpdate
{
    public partial class Form1 : Form
    {
        private string AccessToken = "";
        private string asiBaseAddress = "";
        JArray finalproductitem_array = new JArray();
        JArray finalgroup_array = new JArray();

        DataTable ProductdataTable = new DataTable("ProductItem");
        DataColumn ProductdtColumn;
        DataRow ProductdtRow;

        DataTable GroupdataTable = new DataTable("Group");
        DataColumn GroupdtColumn;
        DataRow GroupdtRow;

        DataTable GroupRoledataTable = new DataTable("GroupRole");
        DataColumn GroupRoledtColumn;
        DataRow GroupRoledtRow;

        public Form1()
        {
            InitializeComponent();
            txtStdPrice.Text = "1";
            dtTermStartDate.Value = DateTime.Now;
            dtTermEndDate.Value = DateTime.Now.AddDays(30);

            asiBaseAddress = System.Configuration.ConfigurationManager.AppSettings["ASIBaseAddress"].ToString();

            lblAsibaseUrl.Text = asiBaseAddress;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SetDataTableColumn();
            SetGroupComboColumn();
            SetGroupRoleComboColumn();

            dgvProductitem.DataSource = ProductdataTable;
            dgvProductitem.Columns["Guid"].Visible = false;

            //--------------------------------------------------------
            GenerateToken();
            //--------------------------------------------------------
            GetProductList();
            SetFinalProductGrid();

            //--------------------------------------------------------
            GetGroups();
            if (finalgroup_array != null)
            {
                if (finalgroup_array.Count > 0)
                {
                    SetGroupComboData();

                    cmbGroup.ValueMember = "GroupId";
                    cmbGroup.DisplayMember = "Name";
                    cmbGroup.DataSource = GroupdataTable;
                }
            }
            //--------------------------------------------------------
        }
        //--------------------------------------------------------
        private void GenerateToken()
        {
            try
            {
                //asiBaseAddress = System.Configuration.ConfigurationManager.AppSettings["ASIBaseAddress"].ToString();
                string asiUserName = System.Configuration.ConfigurationManager.AppSettings["ASIusername"].ToString();
                string asiPassword = System.Configuration.ConfigurationManager.AppSettings["ASIpassword"].ToString();
                string asiGrantType = System.Configuration.ConfigurationManager.AppSettings["ASIgrant_type"].ToString();

                var client = new HttpClient();
                client.BaseAddress = new Uri(asiBaseAddress);
                var request = new HttpRequestMessage(HttpMethod.Post, "token");

                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("username", asiUserName));
                keyValues.Add(new KeyValuePair<string, string>("password", asiPassword));
                keyValues.Add(new KeyValuePair<string, string>("grant_type", asiGrantType));

                request.Content = new FormUrlEncodedContent(keyValues);
                var responseMessage = client.SendAsync(request);
                var responseJSONData = responseMessage.Result.Content.ReadAsStringAsync().Result;

                var Replace_responseJSONData = responseJSONData.Replace(".issued", "issued").Replace(".expires", "expires");

                TokenResponse tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(Replace_responseJSONData);

                // access token is not null and error not occured occured
                if (string.IsNullOrWhiteSpace(tokenResponse.error) && !string.IsNullOrWhiteSpace(tokenResponse.access_token))
                    AccessToken = tokenResponse.access_token;
                else
                    txtResponse.Text = "GenerateToken Error occured : " + tokenResponse.error + "\r\n";
            }
            catch (Exception ex)
            {
                txtResponse.AppendText("GenerateToken Error occured : " + "\r\n");

                if (!string.IsNullOrEmpty(ex.Message))
                    txtResponse.AppendText(ex.Message + "\r\n");
                if (ex.InnerException != null)
                {
                    if (!string.IsNullOrEmpty(ex.InnerException.Message))
                        txtResponse.AppendText("\nInner exception : " + ex.InnerException.Message + "\r\n");
                }
            }
        }

        private void GetGroups(int offset = 0)
        {
            try
            {
                string Groups_ApiUrl = asiBaseAddress + "/api/group?Offset=" + offset;
                var group_client = new RestClient(Groups_ApiUrl);
                var group_request = new RestRequest(Method.GET);

                group_request.AddHeader("cache-control", "no-cache");
                group_request.AddHeader("Content-Type", "application/json");
                group_request.AddHeader("Authorization", "Bearer " + AccessToken);

                IRestResponse group_response = group_client.Execute(group_request);

                if (group_response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JValue group_value = (JValue)group_response.Content;
                    var group_obj = JObject.Parse(group_value.Value.ToString());
                    var group_array = (JArray)group_obj["Items"]["$values"];

                    foreach (var item in group_array)
                    {
                        finalgroup_array.Add(item);
                    }

                    bool hasnext = Convert.ToBoolean(group_obj["HasNext"]);

                    if (hasnext)
                    {
                        offset = Convert.ToInt32(group_obj["NextOffset"]);
                        GetGroups(offset);
                    }
                }
                else
                {
                    txtResponse.AppendText("Call Get Group api error : " + group_response.Content + "\r\n");
                }
            }
            catch (Exception ex)
            {
                txtResponse.AppendText("Call Get Group api error : " + ex.Message + "\r\n");
            }
        }

        private void GetProductList(int Offset = 0, string Name = null)
        {
            try
            {
                string ProductList_ApiUrl = asiBaseAddress + "/api/item?Offset=" + Offset;

                if (!string.IsNullOrWhiteSpace(Name))
                {
                    ProductList_ApiUrl += "&Name=contains:" + Name;
                }

                var productitem_client = new RestClient(ProductList_ApiUrl);
                var productitem_request = new RestRequest(Method.GET);

                productitem_request.AddHeader("cache-control", "no-cache");
                productitem_request.AddHeader("Content-Type", "application/json");
                productitem_request.AddHeader("Authorization", "Bearer " + AccessToken);

                IRestResponse productitem_response = productitem_client.Execute(productitem_request);

                if (productitem_response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JValue productitem_value = (JValue)productitem_response.Content;
                    var productitem_obj = JObject.Parse(productitem_value.Value.ToString());
                    var productitem_array = (JArray)productitem_obj["Items"]["$values"];

                    foreach (var item in productitem_array)
                    {
                        finalproductitem_array.Add(item);
                    }

                    bool hasnext = Convert.ToBoolean(productitem_obj["HasNext"]);

                    if (hasnext)
                    {
                        Offset = Convert.ToInt32(productitem_obj["NextOffset"]);
                        GetProductList(Offset, Name);
                    }
                }
                else
                {
                    txtResponse.AppendText("Call Get Item api error : " + productitem_response.Content + "\r\n");
                }
            }
            catch (Exception ex)
            {
                txtResponse.AppendText("Call Get Item api error : " + ex.Message + "\r\n");
            }
        }
        //--------------------------------------------------------
        private void SetDataTableColumn()
        {
            ProductdtColumn = new DataColumn
            {
                DataType = typeof(bool),
                ColumnName = "Select",
                Caption = "Check the box to update product"
            };
            ProductdataTable.Columns.Add(ProductdtColumn);

            //ProductdtColumn = new DataColumn
            //{
            //    DataType = typeof(int),
            //    ColumnName = "No"
            //};
            //ProductdataTable.Columns.Add(ProductdtColumn);

            ProductdtColumn = new DataColumn
            {
                DataType = typeof(string),
                ColumnName = "Guid"
            };
            ProductdataTable.Columns.Add(ProductdtColumn);

            ProductdtColumn = new DataColumn
            {
                DataType = typeof(string),
                ColumnName = "Name"
            };
            ProductdataTable.Columns.Add(ProductdtColumn);

            ProductdtColumn = new DataColumn
            {
                DataType = typeof(string),
                ColumnName = "Product Code"
            };
            ProductdataTable.Columns.Add(ProductdtColumn);

            ProductdtColumn = new DataColumn
            {
                DataType = typeof(DateTime),
                ColumnName = "EffectiveDate"
            };
            ProductdataTable.Columns.Add(ProductdtColumn);

            ProductdtColumn = new DataColumn
            {
                DataType = typeof(string),
                ColumnName = "Price"
            };
            ProductdataTable.Columns.Add(ProductdtColumn);

            ProductdtColumn = new DataColumn
            {
                DataType = typeof(string),
                ColumnName = "Group Name"
            };
            ProductdataTable.Columns.Add(ProductdtColumn);

            ProductdtColumn = new DataColumn
            {
                DataType = typeof(string),
                ColumnName = "Group Role"
            };
            ProductdataTable.Columns.Add(ProductdtColumn);

            ProductdtColumn = new DataColumn
            {
                DataType = typeof(string),
                ColumnName = "Term Start Date"
            };
            ProductdataTable.Columns.Add(ProductdtColumn);

            ProductdtColumn = new DataColumn
            {
                DataType = typeof(string),
                ColumnName = "Term End Date"
            };
            ProductdataTable.Columns.Add(ProductdtColumn);
        }

        private void SetDataTableRow()
        {
            int i = 1;
            foreach (var item in finalproductitem_array)
            {
                ProductdtRow = ProductdataTable.NewRow();

                //ProductdtRow["No"] = i;
                ProductdtRow["Select"] = false;
                ProductdtRow["Guid"] = Convert.ToString(item["ItemId"]);
                ProductdtRow["Name"] = Convert.ToString(item["Name"]);
                ProductdtRow["Product Code"] = Convert.ToString(item["ItemCode"]);

                string StartDate = Convert.ToString(item["PublishingInformation"]["StartDate"]);

                if (string.IsNullOrWhiteSpace(StartDate))
                {
                    ProductdtRow["EffectiveDate"] = DBNull.Value;
                }
                else
                {
                    ProductdtRow["EffectiveDate"] = Convert.ToDateTime(item["PublishingInformation"]["StartDate"]);
                }

                ProductdtRow["Price"] = Convert.ToString(item["TempDefaultPrice"]);

                if (item["Group"] != null)
                    ProductdtRow["Group Name"] = Convert.ToString(item["Group"]["Name"]);
                else
                    ProductdtRow["Group Name"] = null;

                if (item["GroupRole"] != null)
                    ProductdtRow["Group Role"] = Convert.ToString(item["GroupRole"]["Name"]);
                else
                    ProductdtRow["Group Role"] = null;

                if (item["GroupTermPolicy"] != null)
                {
                    ProductdtRow["Term Start Date"] = Convert.ToString(item["GroupTermPolicy"]["TermBeginDate"]);
                    ProductdtRow["Term End Date"] = Convert.ToString(item["GroupTermPolicy"]["TermEndDate"]);
                }
                else
                {
                    ProductdtRow["Term Start Date"] = null;
                    ProductdtRow["Term End Date"] = null;
                }

                ProductdataTable.Rows.Add(ProductdtRow);
                i += 1;
            }
        }
        //--------------------------------------------------------
        private void SetGroupComboColumn()
        {
            GroupdtColumn = new DataColumn
            {
                DataType = typeof(string),
                ColumnName = "GroupId",
            };
            GroupdataTable.Columns.Add(GroupdtColumn);

            GroupdtColumn = new DataColumn
            {
                DataType = typeof(string),
                ColumnName = "Name"
            };
            GroupdataTable.Columns.Add(GroupdtColumn);
        }

        private void SetGroupComboData()
        {
            foreach (var item in finalgroup_array)
            {
                GroupdtRow = GroupdataTable.NewRow();

                GroupdtRow["GroupId"] = Convert.ToString(item["GroupId"]);
                GroupdtRow["Name"] = Convert.ToString(item["Name"]);

                GroupdataTable.Rows.Add(GroupdtRow);
            }
        }

        //--------------------------------------------------------
        private void SetGroupRoleComboColumn()
        {
            GroupRoledtColumn = new DataColumn
            {
                DataType = typeof(string),
                ColumnName = "RoleId",
            };
            GroupRoledataTable.Columns.Add(GroupRoledtColumn);

            GroupRoledtColumn = new DataColumn
            {
                DataType = typeof(string),
                ColumnName = "Name"
            };
            GroupRoledataTable.Columns.Add(GroupRoledtColumn);
        }

        private void cmbGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            var GroupId = this.cmbGroup.GetItemText(this.cmbGroup.SelectedValue);

            cmbGroupRole.DataSource = null;
            cmbGroupRole.Items.Clear();
            GroupRoledataTable.Clear();

            try
            {
                if (!string.IsNullOrWhiteSpace(GroupId))
                {
                    if (finalgroup_array != null)
                    {
                        if (finalgroup_array.Count > 0)
                        {
                            // Filter Grouprole from finalGroup_array based in groupid.
                            var temp_group_array = finalgroup_array.Where(x => x["GroupId"]?.ToString() == GroupId).FirstOrDefault();

                            var temp_group_role_array = temp_group_array["Roles"]["$values"];

                            if (temp_group_role_array != null)
                            {
                                if (temp_group_role_array.Count() > 0)
                                {
                                    foreach (var item in temp_group_role_array)
                                    {
                                        GroupRoledtRow = GroupRoledataTable.NewRow();

                                        GroupRoledtRow["RoleId"] = Convert.ToString(item["RoleId"]);
                                        GroupRoledtRow["Name"] = Convert.ToString(item["Name"]);

                                        GroupRoledataTable.Rows.Add(GroupRoledtRow);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                txtResponse.AppendText("Error in binding group role : " + ex.Message + "\r\n");
            }

            cmbGroupRole.ValueMember = "RoleId";
            cmbGroupRole.DisplayMember = "Name";
            cmbGroupRole.DataSource = GroupRoledataTable;
        }
        //--------------------------------------------------------
        private void btnUpdateInfo_Click(object sender, EventArgs e)
        {
            txtResponse.Clear();
            txtResponse.Refresh();
            int i = 1;
            int NumofRowSelected = 0;

            if (!Validate())
                return;

            #region Calculate Term Span
            DateTime TermStartDate = dtTermStartDate.Value.Date;
            DateTime TermEndDate = dtTermEndDate.Value.Date;

            int TermSpanInMonth;
            TermSpanInMonth = ((TermEndDate.Year - TermStartDate.Year) * 12) + TermEndDate.Month - TermStartDate.Month;

            if (chkAsgPurToGrp.Checked)
            {
                if (TermEndDate <= TermStartDate)
                {
                    errorProvider1.SetError(dtTermEndDate, "Enter greater date value for term end date");
                    return;
                }
                else
                {
                    if (TermSpanInMonth == 0)
                    {
                        errorProvider1.SetError(dtTermEndDate, "Term span in month of selected Start and End date is zero(0). Please select other End date");
                        return;
                    }
                    else
                        errorProvider1.SetError(dtTermEndDate, "");
                }
            }

            string str_TermStartDate = dtTermStartDate.Value.ToString("yyyy-MM-dd");
            string str_TermEndDate = dtTermEndDate.Value.ToString("yyyy-MM-dd");

            #endregion

            #region Validate Group And Group Role Selection            
            string GroupId = Convert.ToString(cmbGroup.SelectedValue);
            string GroupRoleId = Convert.ToString(cmbGroupRole.SelectedValue);

            string GroupName = cmbGroup.Text;
            string GroupRoleName = cmbGroupRole.Text;

            if (chkAsgPurToGrp.Checked)
            {
                if (string.IsNullOrEmpty(GroupId))
                {
                    errorProvider1.SetError(cmbGroup, "Select group.");
                    return;
                }
                else
                    errorProvider1.SetError(cmbGroup, "");

                if (string.IsNullOrEmpty(GroupRoleId))
                {
                    errorProvider1.SetError(cmbGroupRole, "Select group role.");
                    return;
                }
                else
                    errorProvider1.SetError(cmbGroupRole, "");
            }
            #endregion

            decimal.TryParse(txtStdPrice.Text, out decimal StandardPrice);

            // read put json format file
            JObject jobj = null;

            // read JSON directly from a file                                
            using (StreamReader file = File.OpenText(Application.StartupPath + "\\putapicall.json"))
            {
                using (JsonTextReader reader = new JsonTextReader(file))
                {
                    jobj = (JObject)JToken.ReadFrom(reader);
                }
            }

            if (jobj != null)
            {
                txtResponse.AppendText("Updating Products. Please wait...\r\n");
                // Get gridview checked row
                foreach (DataGridViewRow row in dgvProductitem.Rows)
                {
                    bool isSelected = Convert.ToBoolean(row.Cells["Select"].Value);

                    if (isSelected)
                    {
                        NumofRowSelected += 1;

                        try
                        {
                            JObject tmpjObject = new JObject(jobj);

                            string ProductGuid = Convert.ToString(row.Cells["Guid"].Value);
                            string ItemCode = Convert.ToString(row.Cells["Product Code"].Value);
                            string Name = Convert.ToString(row.Cells["Name"].Value);

                            var tmpFinalProductItem = finalproductitem_array.Where(x => x["ItemId"]?.ToString() == ProductGuid).FirstOrDefault();

                            if (tmpFinalProductItem != null)
                            {
                                var t1_ItemClass = tmpFinalProductItem.SelectToken("ItemClass");

                                if (t1_ItemClass != null)
                                {
                                    tmpjObject.Add("ItemClass", t1_ItemClass);
                                }

                                if (chkAsgPurToGrp.Checked)
                                {
                                    var t_Group = (JObject)tmpFinalProductItem.SelectToken("Group");

                                    if (t_Group != null)
                                    {
                                        var t_GroupClass = t_Group.SelectToken("GroupClass");

                                        if (t_GroupClass != null)
                                        {
                                            JObject t = tmpjObject["Group"] as JObject;
                                            t.Add("GroupClass", t_GroupClass);
                                        }
                                    }
                                }
                            }

                            // call put api
                            tmpjObject["Name"] = Name;
                            tmpjObject["ItemCode"] = ItemCode;
                            tmpjObject["ItemId"] = ProductGuid;
                            tmpjObject["TempDefaultPrice"] = StandardPrice;

                            if (chkAsgPurToGrp.Checked)
                            {
                                //GroupTermPolicy
                                tmpjObject["GroupTermPolicy"]["TermBeginDate"] = str_TermStartDate;
                                tmpjObject["GroupTermPolicy"]["TermEndDate"] = str_TermEndDate;
                                tmpjObject["GroupTermPolicy"]["TermSpan"] = TermSpanInMonth;

                                //Group
                                tmpjObject["Group"]["GroupId"] = GroupId;
                                tmpjObject["Group"]["Name"] = GroupName;

                                //GroupRole
                                tmpjObject["GroupRole"]["RoleId"] = GroupRoleId;
                                tmpjObject["GroupRole"]["Description"] = GroupRoleName;
                                tmpjObject["GroupRole"]["Name"] = GroupRoleName;
                            }
                            else
                            {
                                // Remove Group, GroupRole, GroupTermPolicy
                                var t = (JObject)tmpjObject.SelectToken("");
                                t.Property("Group").Remove();

                                t = (JObject)tmpjObject.SelectToken("");
                                t.Property("GroupRole").Remove();

                                t = (JObject)tmpjObject.SelectToken("");
                                t.Property("GroupTermPolicy").Remove();
                            }

                            string ItemData = Newtonsoft.Json.JsonConvert.SerializeObject(tmpjObject);

                            if (!string.IsNullOrWhiteSpace(ItemData))
                            {
                                // Call Api
                                string ProductItem_ApiUrl = asiBaseAddress + "/api/productitem/" + ProductGuid;
                                var client = new RestClient(ProductItem_ApiUrl);
                                var request = new RestRequest(Method.PUT);

                                request.AddHeader("cache-control", "no-cache");
                                request.AddHeader("Content-Type", "application/json");
                                request.AddHeader("Authorization", "Bearer " + AccessToken);

                                request.AddParameter("undefined", ItemData, ParameterType.RequestBody);
                                IRestResponse response = client.Execute(request);

                                if (response.StatusCode == System.Net.HttpStatusCode.Created || response.StatusCode == System.Net.HttpStatusCode.OK)
                                    txtResponse.AppendText("Row " + i + " Updated" + "\r\n");
                                else
                                    txtResponse.AppendText("Row " + i + " Error : " + response.Content + "\r\n");
                            }
                            else
                                txtResponse.AppendText("Row " + i + " Error : No data found to update" + "\r\n");
                        }
                        catch (Exception ex)
                        {
                            txtResponse.AppendText("Row " + i + " Error : " + ex.Message + "\r\n");
                        }
                    }
                    i += 1;
                }

                txtResponse.AppendText("Selected products updated.\r\n");

                if (NumofRowSelected == 0)
                    MessageBox.Show("Please select any record to update");
            }
            else
                MessageBox.Show("Unable to read putapicall.json file");
        }
        //--------------------------------------------------------
        private void txtStdPrice_Validating(object sender, CancelEventArgs e)
        {
            decimal StandardPrice = 0;

            if (string.IsNullOrWhiteSpace(txtStdPrice.Text))
            {
                e.Cancel = true;
                errorProvider1.SetError(txtStdPrice, "Enter standard price");
            }
            else if (decimal.TryParse(txtStdPrice.Text, out StandardPrice))
            {
                if (StandardPrice <= 0)
                {
                    e.Cancel = true;
                    errorProvider1.SetError(txtStdPrice, "Enter valid standard price.");
                }
                else
                {
                    e.Cancel = false;
                    errorProvider1.SetError(txtStdPrice, "");
                }
            }
            else
            {
                e.Cancel = true;
                txtStdPrice.Focus();
                errorProvider1.SetError(txtStdPrice, "Enter valid standard price.");
            }
        }

        //--------------------------------------------------------
        private void btnFilter_Click(object sender, EventArgs e)
        {
            string Name = txtNameFilter.Text;

            // first make final item array empty
            finalproductitem_array = new JArray();

            dgvProductitem.DataSource = null;
            dgvProductitem.Rows.Clear();
            dgvProductitem.Refresh();

            ProductdataTable.Clear();

            txtResponse.Text = "Applying filters. Please wait...\r\n";

            GetProductList(0, Name);

            SetFinalProductGrid();

            txtResponse.AppendText("Filters applied.\r\n");
        }
        //--------------------------------------------------------
        private void SetFinalProductGrid()
        {
            if (finalproductitem_array != null)
            {
                if (finalproductitem_array.Count > 0)
                {
                    SetDataTableRow();

                    // filter into datatable EffectiveStartDate and EffectiveEndDate wise

                    DataView dv = new DataView(ProductdataTable);
                    string Expression = string.Empty;

                    if (dtFilterEffectiveStartDate.Checked == true)
                    {
                        if (dtFilterEffectiveStartDate.Value != null)
                            Expression = string.Format("EffectiveDate >= #{0}#", dtFilterEffectiveStartDate.Value.ToString("yyyy-MM-dd"));
                    }

                    if (dtFilterEffectiveEndDate.Checked == true)
                    {
                        if (dtFilterEffectiveEndDate.Value != null)
                        {
                            if (string.IsNullOrWhiteSpace(Expression))
                                Expression += string.Format("EffectiveDate <= #{0}#", dtFilterEffectiveEndDate.Value.ToString("yyyy-MM-dd"));
                            else
                                Expression += string.Format(" And EffectiveDate <= #{0}#", dtFilterEffectiveEndDate.Value.ToString("yyyy-MM-dd"));
                        }
                    }

                    if (!string.IsNullOrEmpty(Expression))
                        dv.RowFilter = Expression;

                    dgvProductitem.DataSource = dv;

                    dgvProductitem.Columns["Guid"].Visible = false;
                    //dgvProductitem.Columns["No"].ReadOnly = true;
                    dgvProductitem.Columns["Name"].ReadOnly = true;
                    dgvProductitem.Columns["Product Code"].ReadOnly = true;
                    dgvProductitem.Columns["EffectiveDate"].ReadOnly = true;
                    dgvProductitem.Columns["Price"].ReadOnly = true;

                    dgvProductitem.Columns["Group Name"].ReadOnly = true;
                    dgvProductitem.Columns["Group Role"].ReadOnly = true;
                    dgvProductitem.Columns["Term Start Date"].ReadOnly = true;
                    dgvProductitem.Columns["Term End Date"].ReadOnly = true;
                }
            }
        }
        
        private void chkAsgPurToGrp_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAsgPurToGrp.Checked)
            {
                cmbGroup.Enabled = true;
                cmbGroupRole.Enabled = true;
                dtTermStartDate.Enabled = true;
                dtTermEndDate.Enabled = true;
            }
            else
            {
                cmbGroup.Enabled = false;
                cmbGroupRole.Enabled = false;
                dtTermStartDate.Enabled = false;
                dtTermEndDate.Enabled = false;
            }
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSelectAll.Checked)
            {
                foreach (DataGridViewRow row in dgvProductitem.Rows)
                {
                    row.Cells["Select"].Value = true;
                }
            }
            else
            {
                foreach (DataGridViewRow row in dgvProductitem.Rows)
                {
                    row.Cells["Select"].Value = false;
                }
            }
        }

        public class TokenResponse
        {
            public string access_token { get; set; }
            public string token_type { get; set; }
            public string expires_in { get; set; }
            public string userName { get; set; }
            public string issued { get; set; }
            public string expires { get; set; }
            public string error { get; set; }
        }
    }
}